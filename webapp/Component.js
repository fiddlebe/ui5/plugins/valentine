sap.ui.define(
	[
        "sap/ui/core/UIComponent"
    ],
	function(UIComponent) {
		"use strict";

		/**
		 * @name        be.fiddle.valentine.Component
         * @alias       myCoolPlugin
		 * @instance
		 * @public
		 * @class
         * @todo please replace myCoolPlugin with a meaningfulname.
		 * <p></p>
		 */
		const myCoolPlugin = UIComponent.extend(
			"be.fiddle.valentine.Component",
			/**@lends be.fiddle.valentine.Component.prototype **/ {
				metadata: {
					manifest: "json"
				}
			}
		);

		/**
		 * @method init
		 * @public
		 * @instance
		 * @memberof be.fiddle.valentine.Component
         * <p>The valentine plugin does not require any shellheader item. just an onclick handler </p>
		 * 
		 */
		myCoolPlugin.prototype.init = function(){
			if (UIComponent.prototype.init ) {
				UIComponent.prototype.init.apply(this,arguments);
			}

			//preCreate the heart:
			this._heart = document.createElement("span");
			this._heart.setAttribute("class", "valentine");
			this._heart.innerText = "❤";
			this._heart.id = "valentine";
			
			document.getElementsByTagName("body")[0].appendChild(this._heart);

			//register the onclick handler
			window.addEventListener("click", this.spreadLove.bind(this));
		};	

		/**
		 * @method destroy
		 * @public
		 * @instance
		 * @memberof be.fiddle.valentine.Component
		 * <p>object destructor: remove any pending event handlers and taskrepeaters</p>
		 */
		myCoolPlugin.prototype.destroy = function(){
			window.removeEventListener("click", this.spreadLove);
			document.getElementsByTagName("body")[0].removeChild(this._heart);
			
			UIComponent.prototype.destroy.apply(this, arguments);
		};

		/**
		 * @method spreadLove
		 * @public
		 * @instance
		 * @memberof be.fiddle.valentine.Component
         * @param {event} event
		 * <p>On every click, spread the love</p>
		 */
		myCoolPlugin.prototype.spreadLove = function(event){
			let animation = [
				{ transform: 'scale(0, 0)' }, 
				{ transform: 'scale(3, 3)' },
				{ transform: 'scale(0, 0)' }
			];

			this._heart.style.left = "" + event.clientX + "px";
			this._heart.style.top = "" + event.clientY + "px";
			this._heart.style.display = "inline";

			this._heart.animate( animation, { 
				duration: 1000,
				iterations: 2
			});
		};

		return myCoolPlugin;
	}
);
